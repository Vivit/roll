%{
#include <stdlib.h>
#include <string.h>
#include <ctype.h> 		/* for toupper() and isalpha() in lexident */
#include "roll.tab.h"
#include "dice.h"
#include "state.h" /* for MAX_KEY_LENGTH */

  int lexident();
%}

%%
[0-9]+              { yylval.i = atoi(yytext); return NUM; }
\$[a-zA-Z]{1,15}    {
  int i = lexident();
  if (i) {
    strncpy(yylval.id, yytext+1, i);
    return IDENT;
  } else return ERROR;
                 }

[dD]                { return DEE; }

[ \t]               ;
\n                  { return EOL; }

[\+-]               { strncpy(yylval.o, yytext, 1); return PLUSMINUS; }
[\*/%]              { strncpy(yylval.o, yytext, 1); return TIMESDIVMOD; }
(=|\<|>)            { strncpy(yylval.o, yytext, 1); return RELATION; }
(\<=|>=)            { strncpy(yylval.o, yytext, 2); return RELATION; }
!                   { strncpy(yylval.o, yytext, 1); return BANG; }
(\||\&)             { strncpy(yylval.o, yytext, 1); return BOOLOP; }
:=                  { strncpy(yylval.o, yytext, 2); return ASSIGNMENT; }

\(                  { return PAREN; }
\)                  { return NERAP; }

%%

/* Scan an identifier name.  Fails if the string is too long. */
int
lexident()
{
  int offset;
  for (offset = 1; offset < MAX_KEY_LENGTH; offset++)
    if (!isalpha(yytext[offset])) break;
  if (isalpha(yytext[offset])) return 0;  /* error: identifier too long */
  for (int i = 1; i < offset; i++)
    yytext[i] = toupper(yytext[i]);
  return offset;
}
