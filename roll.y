%{
#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "dice.h"
#include "reporting.h"
#include "state.h"

  void yyerror(const char *msg);
  int yylex();
%}

%error-verbose

%token NUM ERROR PAREN NERAP IDENT EOL 
%left PLUSMINUS
%left TIMESDIVMOD BANG BOOLOP
%left ASSIGNMENT
%nonassoc DEE RELATION

%start script

%union {
  int i;
  char id[16];  /* Identifiers may be no more than 15 characters long (the 16th
                   being reserved for the null terminator) */
  char o[3]; /* NOTE: This array is 3 bytes long beacuse of the max length of
                      binary operators in the regular grammar.  If longer
                      operator tokens are introduced, this array must be
                      lengthened. */
  struct diestruct *d;
}
%type <i> exp NUM test assign var
%type <d> roll
%type <o> PLUSMINUS TIMESDIVMOD BANG BOOLOP RELATION
%type <id> IDENT

%%
script: body {}

body: line | body line {}

roll: exp DEE exp { $$ = d($1, $3); }

exp: roll {
  $$ = sumdice($1);
  rmdice($1);
 } | exp TIMESDIVMOD exp {
   $$ = (!strcmp($2, "*")) ? $1 * $3 :
        (!strcmp($2, "/")) ? $1 / $3 :
        (!strcmp($2, "%")) ? $1 % $3 : 0;
 } | exp PLUSMINUS exp {
   $$ = (!strcmp($2, "+")) ? $1 + $3 :
        (!strcmp($2, "-")) ? $1 - $3 : 0;
 } | NUM { $$ = yylval.i; }

exp: var { $$ = $1; }

exp: PAREN exp NERAP { $$ = $2; }

assign: IDENT ASSIGNMENT exp { assign($1, &$3, INT_T); $$ = $3; }

var: IDENT {
  if (vartype($1) != INT_T) {
    yyerror("Type error.");
    $$ = 0;
  }
  else {
    int i = * (int*) varval($1);
    $$ = i;
  }
}

test: exp RELATION exp {
  $$ = (!strcmp($2, ">")) ? $1 > $3 :
       (!strcmp($2, "<")) ? $1 < $3 :
       (!strcmp($2, "=")) ? $1 == $3 :
       (!strcmp($2, ">=")) ? $1 >= $3 :
       (!strcmp($2, "<=")) ? $1 <= $3 :
       (!strcmp($2, "!=")) ? $1 != $3 :
    -1; // Can't happen
 } | test BOOLOP test {
  $$ = (!strcmp($2, "&")) ? $1 && $3 :
       (!strcmp($2, "|")) ? $1 || $3 :
    -1; // Can't happen
} | BANG test {
  $$ = ! $2;
} | PAREN test NERAP {
  $$ = $2;
}



line: exp EOL { printf("Result: %d\n", $1); } ;
line: assign EOL { printf("Value: %d\n", $1); }
line: test EOL {
  switch ($1) {
  case 1:
    printf("Success!\n");
    break;
  case 0:
    printf("Failure...\n");
    break;
  default: // Can't happen
    printf("Whoops!\n");
    break;
  }
}
line: error ;
%%

int
main(argc, argv)
     int argc;
     char **argv;
{
  init_roll_state();
  srand(time(NULL));
  verbose = 1;
  yyparse();
  free_roll_state();
  return 0;
}

void
yyerror(msg)
     const char *msg;
{
  printf("ERROR: %s\n", msg);
  return;
}
