#include <stdio.h>
#include <string.h>
#include "reporting.h"
#include "dice.h"

int verbose;

void
msg(v, t)
  const char *v, *t;
{
  printf("%s", verbose ? v : t);
}

void
listdice(d)
  const DICE *d;
{
  if (!verbose) return;  // Do we want this here?
  d = d->head;
  printf("Rolled %d", d->value);
  while (d = d->next, d) printf(", %d", d->value);
  printf("\n");
}
