#include <stdlib.h>
#include "dice.h"
#include "reporting.h"

DICE *mkdice(int,int);

DICE
*d(dice, sides)
  int dice, sides;
{
  int sum = 0;
  DICE *die = mkdice(dice, sides);
  while (sum += rolldie(die), die->next) die = die->next;
  msg("", "Rolled ");
  listdice(die);
  msg("", "\n");
  return die;
}

DICE *
mkdice(dice, sides)
  int dice, sides;
{
  if (sides <= 0) return NULL;

  DICE *die = malloc(sizeof (DICE));
  *die = (DICE) { sides, 0, NULL, die };
  for (int i = 1; i < dice; i++) {
    die->next = malloc(sizeof (DICE));
    *die->next = (DICE) { sides, 0, NULL, die->head };
    die = die->next;
  }

  return die->head;
}

void
rmdice(die)
  DICE *die;
{
  die = die->head;
  DICE *dtmp;
  do {
    dtmp = die;
    die = die->next;
    free(dtmp);
  } while (die);
}

int
sumdice(die)
  const DICE *die;
{
  int sum = 0;
  die = die->head;
  do sum += die->value; while (die = die->next, die);
  return sum;
}

void
rolldice(die)
  DICE *die;
{
  die = die->head;
  do rolldie(die); while (die = die->next, die);
}

int
rolldie(die)
  DICE *die;
{
  return die->value = rand() % die->sides + 1;
}
