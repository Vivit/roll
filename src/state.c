#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "state.h"

unsigned int hash(const char*);

struct entry *lookup(struct table*,const char*);
struct entry *addentry(struct table*,const char*);
struct entry *roll_write(struct entry*,const void*,enum type);
struct entry *clear_entry(struct entry*);

void rmentry(struct entry*);
void rmentries(struct table*,unsigned int);

struct table *state;

#define TABLE_SIZE 4096

/*** Initialization and termination of global state ***/

void
init_roll_state()
{
  state = malloc(sizeof (struct table));
  state->entries = malloc(sizeof (struct entry*) * TABLE_SIZE);
  for (int i = 0; i < TABLE_SIZE; i++) {
    state->entries[i] = NULL;
  }
}


void
free_roll_state()
{
  for (int i = 0; i < TABLE_SIZE; i++) {
    rmentries(state, i);
  }
}

/*** Functions for actually interacting with the state ***/

void *
assign(key, data, t)
     const char *key;
     const void *data;
     enum type t;
{
  struct entry *e = addentry(state, key);
  roll_write(e, data, t);
  return e->data;
}

enum type
vartype (key)
     const char *key;
{
  struct entry *e = lookup(state, key);
  return e ? e->t : VOID_T;
}

const void *
varval(key)
     const char *key;
{
  return lookup(state, key)->data;
}

/*** General hash table functionality ***/

unsigned int
hash(s)
    const char *s;
{
  unsigned long long ret = 0;
  for (int i = 0; i < s[i]; i++) {
    ret += s[i] * pow(26, i);
  }
  return (int) (ret % TABLE_SIZE);
}

/* Adds an entry to the given table at the given key if it does not exist, or
   returns the entry if it does exist. */
struct entry *
addentry(tab, key)
     struct table *tab;
     const char *key;
{
  if (lookup(tab, key)) return lookup(tab,key);
  unsigned int h = hash(key);
  struct entry *e = malloc(sizeof (struct entry));
  e->t = VOID_T;
  strcpy((char*) (e->key), key);
  e->data = NULL;
  e->tab = tab;
  e->next = e->prev = NULL;
  if (tab->entries[h]) {
    e->next = tab->entries[h];
    (tab->entries[h])->prev = e;
  }
  tab->entries[h] = e;
  return e;
}


/* Hash table lookup function */
struct entry *
lookup(tab, key)
     struct table *tab;
     const char *key;
{
  unsigned int h = hash(key);
  struct entry *e = tab->entries[h];
  while (e) {
    if (!strcmp(key, e->key)) break;
    e = e->next;
  }
  return e;
}

struct entry *
roll_write(e, data, t)
    struct entry *e;
    const void *data;
    enum type t;
{
  clear_entry(e);
  e->t = t;
  switch (t) {
  case INT_T:
  default:
    e->data = malloc(sizeof (int));
    *(int*) e->data = *(int*) data;
    break;
  case STR_T:
    e->data = malloc(1 + strlen((char*) data));
    strcpy(e->data, (char*) data);
    }
  return e;
}

struct entry *
clear_entry(e)
     struct entry *e;
{
  if (!e->data) return e;
  switch (e->t) {
  case INT_T:
  default:
    free((int*) e->data);
    break;
  case STR_T:
    free((char*) e->data);
    break;
  }
  e->t = VOID_T;
  e->data = NULL;
  return e;
}

void
rmentry(e)
    struct entry *e;
{
    if (e->next) e->next->prev = e->prev;
    if (e->prev) e->prev->next = e->next;
    else e->tab->entries[hash(e->key)] = e->next;
    clear_entry(e);
    free(e);
}

void
rmentries(tab, h)
    struct table *tab;
    unsigned int h;
{
    while (tab->entries[h]) rmentry(tab->entries[h]);
}
