ROOT=$(shell pwd)

SRC=$(ROOT)/src
BIN=$(ROOT)/bin
DOC=$(ROOT)/doc
INCLUDE=$(ROOT)/include
PROGRAM=roll

all: c

# Compile program
CC=gcc
CSRC=$(SRC)/*.c
CCFLAGS=-g
CWFLAGS=-Wall -Wpedantic
CLFLAGS=-lfl -lm
CCOUT=bin/$(PROGRAM)

c: lex yacc
	$(CC) $(CCFLAGS) -I $(INCLUDE) $(CWFLAGS) $(CSRC) -o $(CCOUT) $(CLFLAGS)
	chmod u+x $(BIN)/$(PROGRAM)

# Compile lexer
LEX=flex
LCFLAGS=-v -L #-d
LOUT=$(PROGRAM).yy.c
lex:
	$(LEX) $(LCFLAGS) -o $(SRC)/$(LOUT) $(PROGRAM).lex

# Compile parser
YACC=bison
YCFLAGS=-t -v
YCOUT=$(PROGRAM).tab.c
YREPORT=--report-file=$(DOC)/$(PROGRAM).output --graph=$(DOC)/$(PROGRAM).graph
YHOUT=$(PROGRAM).tab.h
yacc:
	$(YACC) $(YCFLAGS) --defines=$(INCLUDE)/$(YHOUT) $(YREPORT) -o $(SRC)/$(YCOUT) $(PROGRAM).y 

clean:
	rm -f $(INCLUDE)/$(YHOUT)
	rm -f $(SRC)/$(YCOUT)
	rm -f $(SRC)/$(LOUT)
	rm -f $(DOC)/$(PROGRAM).graph
	rm -f $(DOC)/$(PROGRAM).output
