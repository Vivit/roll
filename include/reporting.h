#ifndef __REPORTING__
#define __REPORTING__

#include "dice.h"

extern int verbose;

void msg(const char*, const char*);
void listdice(const DICE *d);

#endif
