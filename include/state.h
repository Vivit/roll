#ifndef __STATE__
#define __STATE__

#define MAX_KEY_LENGTH 16

enum type {
  VOID_T,
  INT_T,
  STR_T,
  NUM_TYPES
};

struct table;
struct entry;
struct entry {
  enum type t;
  const char key[MAX_KEY_LENGTH];
  void *data;
  struct table *tab;
  /* Linked list for separate chaining */
  struct entry *next;
  struct entry *prev;
};

struct table {
  struct entry **entries;
};

extern struct table *state;

void init_roll_state();
void free_roll_state();

void *assign(const char*,const void*,enum type);
enum type vartype(const char*);
const void *varval(const char*);

#endif /* __STATE__ */

