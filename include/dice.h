#ifndef __DICE__
#define __DICE__

struct diestruct;

typedef struct diestruct {
    unsigned int sides;
    unsigned int value;
    struct diestruct *next;
    struct diestruct *head;
} DICE;

DICE *d(int,int);
int sumdice(const DICE*);
int rolldie(DICE*);
void rmdice(DICE*);

#endif
